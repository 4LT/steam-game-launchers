#!/usr/bin/env wish

package require Tk

set boolEnvVars [list PROTON_LOG PROTON_DUMP_DEBUG_COMMANDS\
    PROTON_USE_WINED3D PROTON_NO_D3D11 PROTON_NO_D3D10 PROTON_NO_ESYNC\
    PROTON_FORCE_LARGE_ADDRESS_AWARE PROTON_OLD_GL_STRING]
set strEnvVars [list PROTON_DEBUG_DIR]

set proton [lindex $argv 0]
set pfx [file join $env(STEAM_COMPAT_DATA_PATH) "pfx"]
set regedit [file join $pfx "drive_c/windows/regedit.exe"]
set winecfg [file join $pfx "drive_c/windows/syswow64/winecfg.exe"]
set exe [lindex $argv 2]
set boolVarDict [dict create]
set strVarDict [dict create]
    
foreach var $boolEnvVars {
    if {[info exists env($var)]} {
        set val $env($var)
    } else {
        set val 0
    }
    
    dict set boolVarDict $var $val
}

foreach var $strEnvVars {
    if {[info exists env($var)]} {
        set val $env($var)
    } else {
        set val ""
    }
    
    dict set strVarDict $var $val
}

grid [ttk::frame .chks]
grid [ttk::frame .entries]
grid [ttk::frame .buttons]
grid [ttk::button .buttons.regedit -text "Regedit" -command regedit]\
    [ttk::button .buttons.winecfg -text "Winecfg" -command winecfg]\
    [ttk::button .buttons.play -text "Play" -command play]

grid columnconfigure . "all" -pad 16
grid rowconfigure . "all" -pad 16
grid columnconfigure .entries 0 -pad 8
grid columnconfigure .buttons "all" -pad 8

wm resizable . 0 0
wm title . "Proton Configurator"
update
set x [expr ([winfo screenwidth . ] - [winfo width . ]) / 2]
set y [expr ([winfo screenheight . ] - [winfo height . ]) / 2]
wm geometry . "+$x+$y"

dict for {key val} $boolVarDict {
    set checkVar$key $val
    grid [ttk::checkbutton .chks.c$key -text $key -variable checkVar$key]\
        -sticky w
}

dict for {key val} $strVarDict {
    set entryVar$key $val
    grid [ttk::label .entries.l$key -text $key]\
        [ttk::entry .entries.e$key -textvariable entryVar$key]\
        -sticky w
}

proc play {} {
    foreach var $::boolEnvVars {
        set val [subst \$::checkVar$var]
        set ::env($var) $val
    }

    foreach var $::strEnvVars {
        set val [subst \$::entryVar$var]
        if {$val != ""} {
            set ::env($var) $val
        } elseif {[info exists ::env($var)]} {
            unset ::env($var)
        }
    }

    exec $::proton waitforexitandrun $::exe &
    destroy .
}

proc regedit {} {
    exec $::proton waitforexitandrun $::regedit &
}

proc winecfg {} {
    exec $::proton waitforexitandrun $::winecfg &
}
