steam-game-launchers
===

A collection of front-ends targeted for Linux to configure and launch various
Steam games.  To use, set the launch options of a game to
`/path/to/script %command%`.

wa-launcher.tcl
---

Worms: Armageddon launcher.  Allows resetting game configuration to default 
in case the game becomes unusable.  Also provides access to the sound bank
editor.

proton-launcher.tcl
---

Configure Proton env vars before launching a game.
