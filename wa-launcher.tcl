#!/usr/bin/env wish
 
# To use, set W:A launch options to "path/to/wa-launcher.tcl %command%"

package require Tk

set proton [lindex $argv 0]
set pfx [file join $env(STEAM_COMPAT_DATA_PATH) "pfx"]
set regedit [file join $pfx "drive_c/windows/regedit.exe"]
set waExec [lindex $argv 2]
set waDir [file dirname $waExec]
set waTweakDir [file join $waDir "Tweaks"]
set waBankEditor [file join $waDir "User/BankEditor.exe"]
set resetReg [file join $waTweakDir "ResetRegistryOptions.reg"]

grid [ttk::button .play -text "Play W:A" -command play]
grid [ttk::button .reset -text "Reset Config" -command reset] 
grid [ttk::button .regedit -text "Regedit" -command regedit] 
grid [ttk::button .edit -text "Sound Bank Editor" -command bankEdit]

grid rowconfigure . all -pad 16
grid columnconfigure . all -pad 16

wm resizable . 0 0
wm title . "W:A Launcher"
update
set x [expr ([winfo screenwidth . ] - [winfo width . ]) / 2]
set y [expr ([winfo screenheight . ] - [winfo height . ]) / 2]
wm geometry . "+$x+$y"

proc play {} {
    exec $::proton waitforexitandrun $::waExec &
    destroy .
}

proc reset {} {
    exec -ignorestderr $::proton waitforexitandrun $::regedit /S $::resetReg
}

proc regedit {} {
    exec $::proton waitforexitandrun $::regedit &
}

proc bankEdit {} {
    set oldWD [pwd]
    cd [file join $::waDir User]
    exec $::proton waitforexitandrun "BankEditor.exe" &
    cd $oldWD
}
